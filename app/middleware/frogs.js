import request from 'superagent';
import Promise, { using } from 'bluebird';
import _ from 'lodash';
import config from 'config';


var serviceFrog = {},
  APIUrl = '/api/frogs/';

// This responds a GET request for get a frog
serviceFrog.get = function(id, success, error){
  request
    .get(APIUrl + id)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a POST request for create new frog
serviceFrog.post = function(data, success, error){
  request
    .post(APIUrl)
    .send(data)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a PUT request for update frog
serviceFrog.put = function(data, success, error){
  request
    .put(APIUrl)
    .send(data)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a GET request for the frog listing.
serviceFrog.list = function(success, error){
  request
    .get(APIUrl)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a DELETE request for a frog.
serviceFrog.del = function(id, success, error){
  request
    .del(APIUrl + id)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a GET request for search frogs.
serviceFrog.search = function(data, success, error){
  request
    .post(APIUrl + 'search')
    .send(data)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

module.exports = serviceFrog;