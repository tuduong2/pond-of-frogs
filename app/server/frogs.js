var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;

// This responds a GET request for get a frog
router.get('/:id', function (req, res) {
   // Prepare output in JSON format
   var db = req.db;
   var collection = db.get('frogs');
   var object_id = req.params.id;
   
   collection.findOne({ '_id' : object_id }, function(err, result){
        res.send(
            (err === null) ? result : { msg: 'error: ' + err }
        );
   });
});

// This responds a POST request for create new frog
router.post('/', function (req, res) {
   // Prepare output in JSON format
   var db = req.db;
   var collection = db.get('frogs');
   var response = {
       pond_environment: req.body.pond_environment,
       mating: req.body.mating,
       gender:req.body.gender,
       name: req.body.name,
       birth: req.body.birth
   };
   
   collection.insert(response, function(err, result){
        res.send(
            (err === null) ? { msg: result } : { msg: 'error: ' + err }
        );
   });
});

// This responds a PUT request for update frog
router.put('/', function (req, res) {
   var db = req.db;
   var collection = db.get('frogs');
   var object_id = req.body._id;
   collection.update({ '_id': object_id}, req.body ,function(err, result){
        res.send(
            (err === null) ? { msg: "Update successfully" } : { msg: 'error: ' + err }
        );
   });
});

// This responds a DELETE request for a frog.
router.delete('/:id', function (req, res) {
   var db = req.db;
   var collection = db.get('frogs');
   var object_id = req.params.id;

   //Delete frog mating
   collection.find({mating: object_id} ,{}, function(err, result){
      if(!err){
        result.map(function(item, i){
          var indexOf = item.mating.indexOf(object_id);
          if(indexOf > -1){
            item.mating.splice(indexOf, 1);
            collection.update({ '_id': item._id}, item);
          }
        });
      }
   });

   //Delete frog
   collection.remove({ '_id' : object_id }, function(err) {
     res.send((err === null) ? { msg: "Delete successfully" } : { msg:'error: ' + err });
   });
});

// This responds a GET request for the frog listing.
router.get('/', function (req, res) {
    var db = req.db;
    var collection = db.get('frogs');
    collection.find({} ,{},function(err ,result){
        res.send(
            (err === null) ? { result } : { msg: 'error: ' + err }
        );
    });
});

// This responds a GET request for search frogs.
router.post('/search', function (req, res) {
    var db = req.db;
    var collection = db.get('frogs');
    var ids = req.body.ids;
    console.log('ids', ids);
    var obj_ids = [];
    ids.map(function(item, i){
      obj_ids.push(new ObjectId(item));
    });
    collection.find({'_id' : {$in : obj_ids}} ,{},function(err ,result){
      console.log('result', result);
         res.send(
            (err === null) ? { result } : { msg: 'error: ' + err }
        );
    });
});

module.exports = router;
