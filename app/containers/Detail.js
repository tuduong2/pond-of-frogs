import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import serviceFrog from '../middleware/frogs';

const Detail = React.createClass({

  getInitialState: function(){
    return {
      frogs: [],
      isFrogsLoading: true,
      isMatingLoading: true
    };
  },

  componentDidMount: function() {
    //Call API to get frog listing
    serviceFrog.get(this.props.params.id, function(res){
      var frog = res.body;
      this.setState({
        frog: res.body,
        isFrogLoading: false,
        isFrogLoaded: true
      });

      if(frog && frog.mating){
        //Call API to get mating detail
        serviceFrog.search({ids: frog.mating}, function(res){
          console.log(res.body.result);
          this.setState({
            matings: res.body.result,
            isMatingLoading: false,
            isMatingLoaded: true
          });
        }.bind(this));
      }

    }.bind(this), function(err){
      console.log('err', err);
    }.bind(this));
  },

  handleDelete: function() {
    e.preventDefault();
    if(confirm("Are you sure you want to delete?")){
      //Call API to delete frog listing
      serviceFrog.del(this.state.frog._id, function(res){
        document.location.href = '/';
      }.bind(this), function(err){
        console.log('err', err);
      }.bind(this));
    }
  },  
  
  render: function() {
    return (
      <div className="listing">
        <h3>Frog Detail</h3>
        <hr/>
        {this.state.isFrogLoading &&
          <i className="glyphicon glyphicon-refresh spinning"></i>
        }
        {this.state.isFrogLoaded &&
          <div>
            <table className="table table-bordered">
              <tbody>
                <tr>
                  <td width="200">Name</td>
                  <td>{this.state.frog.name}</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td>{this.state.frog.gender}</td>
                </tr>
                <tr>
                  <td>Birth</td>
                  <td>{this.state.frog.birth}</td>
                </tr>
                <tr>
                  <td>Pond Environment</td>
                  <td>{this.state.frog.pond_environment}</td>
                </tr>
                <tr>
                  <td>Mating</td>
                  <td>
                    {this.state.isMatingLoaded && this.state.matings.length == 0 && <div>Not found</div>}
                    {this.state.matings && this.state.matings.map(function(item, i){
                      return(
                        <div key={i}>- {item.name}</div>
                      )
                    }, this)}
                  </td>
                </tr>
              </tbody>
            </table>
            <hr/>
            <div>
              <a href={"/frogs/edit/" + this.state.frog._id} className="btn btn-primary" onClick={this.handleSubmit}>
                Edit
              </a>
              &nbsp;
              <a className="btn btn-danger" onClick={this.handleDelete}>
                Delete
              </a>
              &nbsp;
              <a href="/" className="btn btn-info">
                Back
              </a>
            </div>
          </div>
        }
      </div>
    );
  }
});

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Detail);