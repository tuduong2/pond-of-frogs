# Pond of Frogs APP
A demo app use Redux + React-router + Express

## How to run this app?

- clone this app and name it as whatever your want:
`$ git clone https://bitbucket.org/tuduong2/pond-of-frogs`

- Install dependencies:
`$ npm install`

- Host dev environment and start to build something chaning the world!
`$ npm start`

## Features:
- Universal rendering, with async data support
- Server side redirect
- Separate vendor and app js files

## Stack:
- React 15.0.2
- React-Router 1.0.2
- Express as isomorphic server
- Babel
- Webpack
- Tiny Validator (for v4 JSON Schema)
- MongoDB via https://mlab.com/
- Monk

## Routes:
- Listing page: `{base_url}`
- Add new Frog page: `{base_url}/frogs/add`
- Edit Frog page: `{base_url}/frogs/edit/:id`
- Detail page: `{base_url}/frogs/:id`