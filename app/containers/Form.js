import React, { Component } from 'react';
import ReactDOM, {Input} from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import DeepLinkedStateMixin from 'react-deep-link-state';
import DatePicker from 'react-datepicker';
import Moment from 'moment';
import serviceFrog from '../middleware/frogs';
import MultiSelect from 'react-bootstrap-multiselect';
import SelectBox from '../components/select-box';
import tv4 from 'tv4';

/**
  tv4 custom message
*/
tv4.setErrorReporter(function (error, data, schema) {
  if(schema.errorMessage && schema.errorMessage[error.code]){
      return schema.errorMessage[error.code];
  }
  return;
});

var pondEnvironments = ['Europe', 'Asia', 'North America', 'South America', 'Africa' , 'Oceania'],
    genders = ['Male', 'Female'];

const Form = React.createClass({

  mixins: [DeepLinkedStateMixin],

  getInitialState: function(){
    return {
      data: {
        name: '',
        birth: '',
        gender: genders[0],
        pond_environment: pondEnvironments[0],
        mating: []
      },
      frogs: [],
      error: {}
    };
  },

  componentDidMount() {
    if(this.props.params.id){
      //Call API to get frog detail
      serviceFrog.get(this.props.params.id, function(res){
        var data = res.body;
        if(data){
          this.setState({
            data: res.body,
            birth: Moment(data.birth)
          });
        }
      }.bind(this), function(err){
        console.log('err', err);
      }.bind(this));
    }

    //get all frogs
    serviceFrog.list(function(res){
      console.log('res.body.result', res.body.result);
      var frogs = res.body.result;
      frogs.map(function(item, i){
        if(item._id == this.props.params.id){
          frogs.splice(i, 1);
        }
      }, this);
      this.setState({
        frogs: frogs
      });
    }.bind(this), function(err){
      console.log('err', err);
    }.bind(this));
  },

  handleDatePickerChange: function(value){
    this.state.birth = value;
    this.state.error = {};
    this.setState(this.state);
  },

  handleMatingChange: function(value){
    console.log('e.target.value', value);
  },

  handleMultiChange: function (mating) {
    this.state.data.mating = mating;
    this.setState(this.state)
  },

  handleDataChange: function(field, e){
    //reset error
    this.state.data[field] = e.target.value;
    this.state.error = {};
    this.setState(this.state);
  },

  getErrorKey: function(error){
    return error.params.key ? error.params.key : (error.dataPath ? error.dataPath.replace('/', '') : '');
  },

  handleSubmit: function(){
    //reset error
    this.setState({error: {}});

    var data = this.state.data,
        frogSchema = {
          "type": "object",
          "required": ["name", "birth", "gender", "pond_environment"],
          "properties": {
          		"name": {
          			"type": "string",
          			"minLength": 1,
          			"errorMessage": {
          			  "200": "Please enter name."
          			}
          		},
          		"birth": {
          			"type": "string",
          			"pattern": "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$",
          			"errorMessage": {
                  "202": "Please enter a valid birthday (YYYY-MM-DD)."
                }
          		},
          		"gender": {
          			"type": "string"
          		},
          		"pond_environment": {
          			"type": "string"
          		}
          	}
        },
        validate;

    data.birth = this.state.birth ? this.state.birth.format('YYYY-MM-DD') : '',

    validate = tv4.validateResult(data, frogSchema);

    if(!validate.valid){
      var key = this.getErrorKey(validate.error),
          ref = this.refs[key],
          message = validate.error.message,
          error = {};

      error[key] = validate.error.message;
      this.setState({error: error});

      if(key == 'birth'){
        ref = this.refs[key].refs['input'];
      }
      ReactDOM.findDOMNode(ref).focus();

      return;
    }

    //Form is Valid
    if(this.state.data._id){
      //Call API to update frog
      serviceFrog.put(data, function(res){
        alert(res.body.msg);
        document.location.href = '/';
      }.bind(this), function(err){
        alert(err.body.msg);
      }.bind(this));
    } else {
      //Call API to create new frog
      serviceFrog.post(data, function(res){
        alert('Created successfully.');
        document.location.href = '/';
      }.bind(this), function(err){
        alert(err.body.msg);
      }.bind(this));
    }
  },

  render: function() {
    return (
      <div className="listing">
        <h3>{this.state.data.id ? 'Edit Frog' : 'Add New Frog'}</h3>
        <hr/>
        <form className="form form-vertical">
          <div className="control-group">
            <label>Name *</label>
            <div className="controls">
              <input ref="name" type="text" className="form-control" value={this.state.data.name} onChange={this.handleDataChange.bind(this, 'name')}/>
              {this.state.error.name && <small className="text-danger">{this.state.error.name}</small>}
            </div>
          </div>
          <div className="control-group">
            <label>Birthday *</label>
            <div className="controls">
              <DatePicker
                ref="birth"
                className="form-control"
                dateFormat="YYYY-MM-DD"
                maxDate={Moment()}
                selected={this.state.birth}
                onChange={this.handleDatePickerChange} />
              {this.state.error.birth && <small className="text-danger">{this.state.error.birth}</small>}
            </div>
          </div>
          <div className="control-group">
            <label>Gender *</label>
            <div className="controls">
              <select ref="gender" className="form-control" value={this.state.data.gender} onChange={this.handleDataChange.bind(this, 'gender')} >
                {genders.map(function(item, i){
                  return (
                    <option value={item} key={i}>{item}</option>
                  )
                }, this)}
              </select>
            </div>
          </div>
          <div className="control-group">
            <label>Pond Environment *</label>
            <div className="controls">
              <select ref="pond_environment" className="form-control" value={this.state.data.gender} onChange={this.handleDataChange.bind(this, 'pond_environment')}>
                {pondEnvironments.map(function(item, i){
                  return (
                    <option value={item} key={i}>{item}</option>
                  )
                }, this)}
              </select>
            </div>
          </div>

          <div className="control-group">
              <label>Mating</label>
            <div className="controls">
              <SelectBox label="Select..." multiple={true} onChange={this.handleMultiChange} value={this.state.data.mating}>
                {this.state.frogs && this.state.frogs.map(function(item, i){
                  return (
                    <option value={item._id} key={i}>{item.name}</option>
                  )
                }, this)}
                </SelectBox>
            </div>
          </div>

          <div className="control-group">
            <label></label>
            <div className="controls">
              <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>
                Post
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
});

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Form);